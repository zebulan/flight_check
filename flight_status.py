from bs4 import BeautifulSoup
from json import load
from os import path
from random import choice, randint
from requests import get, post
from scrape_airport_codes import iata_scraper, filename
from time import localtime
from user_agents import random_ua


def minutes_to_hours(minutes):  # timedelta instead?? seconds to hours???
    """
    :param minutes: String of total duration, in minutes, of a flight
    :return: String in the form 'hour(s) minute(s)'
    """
    h, m = divmod(int(minutes), 60)
    h_str, m_str = 'hours', 'minutes'
    if h == 1:  # single hour
        h_str = 'hour'
    if m == 1:  # single minute
        m_str = 'minute'
    if h == 0:  # zero hours, just return minutes
        return '{} {}'.format(m, m_str)
    return '{} {} {} {}'.format(h, h_str, m, m_str)


def time_slice(time):
    """
    :param time: String of time in format '%Y-%m-%dT%H:%M:%S.000Z'
    :return: String of time in format '%H:%M:%S'
    """
    return time[time.find('T')+1:time.rfind('.')]


def hms_to_minutes(time):
    """
    :param time: String of time in format '%H:%M:%S'
    :return: Integer of given time converted to minutes
    """
    h, m = (int(a) for a in time.split(':')[:-1])
    return h * 60 + m


def time_diff(sch_time, est_time):
    """
    :param sch_time: String of time in format '%H:%M:%S' (Scheduled)
    :param est_time: String of time in format '%H:%M:%S' (Estimated)
    :return: String indicating how early/late flight is.
             Returns a blank string if plane is on time.
    """

    diff = hms_to_minutes(est_time) - hms_to_minutes(sch_time)
    if diff == 0:
        return ''   # no difference
    elif diff < 0:
        return '- EARLY by {}'.format(minutes_to_hours(abs(diff)))  # early
    return '- {}'.format(minutes_to_hours(diff))  # late


def convert_iata(iata):
    """
    :param iata: Airport code
    :return: String of corresponding city (two options; Airport & Location)
    """
    return airport_codes[iata]['Location']


def air_canada_flight_info(flight_num):
    """
    :param flight_num: Integer of an Air Canada flight number
    :return: BeautifulSoup object, XML format
    """
    date = localtime()
    today = '{:0>2}-{:0>2}-{}'.format(date.tm_mon, date.tm_mday, date.tm_year)
    ac = 'http://services.aircanada.com/portal/rest/getFlightsByFlightNumber'

    payload = {'forceTimetable': 'true',
               'flightNumber': flight_num,
               'carrierCode': 'AC',
               'date': today,
               'cache': randint(1000, 50000),
               'app_key': 'AE919FDCC80311DF9BABC975DFD72085'}

    headers = {'Host': 'services.aircanada.com',
               'User-Agent': choice(user_agents),  # random user-agent
               'Referer': 'http://services.aircanada.com/portal-web/EasyXDM/cors/index.html?xdm_e=http%3A%2F%2Fwww.aircanada.com&xdm_c=default263&xdm_p=1',
               'Connection': 'keep-alive'}

    r = get(ac, params=payload, headers=headers)
    soup = BeautifulSoup(r.text, 'xml')

    if not r.ok or soup.Flights is None:  # error handling
        get_flight_num()

    # print(soup.prettify())    # show the xml
    air_canada_print_flight(soup)


def air_canada_print_flight(xml):
    """
    :return: Prints strings of corresponding information from an
             Air Canada flight number
    """
    dep_ap = xml.DepartureStationInfo
    d_sch = time_slice(dep_ap.ScheduledTime.text)  # scheduled departure time
    d_est = time_slice(dep_ap.EstimatedTime.text)  # estimated departure time
    dep_diff = time_diff(d_sch, d_est)

    arr_ap = xml.ArrivalStationInfo
    a_sch = time_slice(arr_ap.ScheduledTime.text)  # scheduled arrival time
    a_est = time_slice(arr_ap.EstimatedTime.text)  # estimated arrival time
    arr_diff = time_diff(a_sch, a_est)

    try:  # flight status msg(s), always overall, detailed sometimes
        detailed = xml.DetailedStatus.text
        msg = {'DEPARTED_OFF_BLOCK': 'Left the Gate',
               'AIRBORN_IN_FLIGHT': 'In Flight',
               'TOUCHDOWN_LANDED': 'Landed',
               'ARRIVED_ON_BLOCK': 'Arrived at Gate'}
        if detailed in msg:  # swap msg ONLY if a match in msg dict
            detailed = msg[detailed]
        status = '{} - {}'.format(xml.OverallStatus.text, detailed)
    except AttributeError:
        status = '{}'.format(xml.OverallStatus.text)

    print('-' * 10)
    # print('{} -> {}'.format(dep_ap.Airport.text, arr_ap.Airport.text))
    print('Flight:    {}'.format(xml.FlightNumber.text))
    print('Status:    {}'.format(status))
    print('Distance:  {} km'.format(xml.Distance.text))
    print('Duration:  {}'.format(minutes_to_hours(xml.Duration.text)))
    print()
    print('Depart:    {}'.format(convert_iata(dep_ap.Airport.text)))
    print('Status:    {} {}'.format(dep_ap.Status.text, dep_diff))
    # print('Scheduled Depart:   {}'.format(d_sch))
    print('Time:      {}'.format(d_est))
    print()
    print('Arrive:    {}'.format(convert_iata(arr_ap.Airport.text)))
    print('Status:    {} {}'.format(arr_ap.Status.text, arr_diff))
    # print('Scheduled Arrival:  {}'.format(a_sch))
    print('Time:      {}'.format(a_est))

    # then add in countdown from current time to arrival
    # current time might work for me but DOESN'T work if
    # I was to look up arrival times for another airport
    # in a different timezone...

    # lessen output if the flight HASN'T left yet OR has landed


def westjet_flight_info(flight_num):
    westjet = 'https://www.westjet.com/guest/en/flightStatusRequest.shtml'
    headers = {'Host': 'www.westjet.com', 'User-Agent': choice(user_agents),
               'Referer': 'https://www.westjet.com/guest/en/flightStatusRequest.shtml'}

    payload = {'searchType': 'flightNumberSearch',
               'locale': 'en',
               'date': '0',  # -1 = yesterday, 0 = today, 1 = tomorrow
               'flightNumber': flight_num}

    r = post(westjet, params=payload, headers=headers)
    soup = BeautifulSoup(r.text)

    if not r.ok or soup.find('table') is None:  # error handling
        get_flight_num()

    td = ['city-data', 'flight-status', 'time-data']
    flight_info = list()
    for class_value in td:
        flight_info.append([' '.join(a.text.split()) for a in
                            soup.find_all('td', {'class': class_value})])

    pairs = list(zip(*flight_info))
    for b in range(0, len(pairs), 2):  # handles single or multiple
        westjet_print_flight(flight_num, pairs[b], pairs[b+1])


def westjet_print_flight(fn, dep, arr):
    dep_ap, dep_status, dep_sch = dep
    arr_ap, arr_status, arr_sch = arr

    print('-' * 10)
    print('Flight:  {}'.format(fn))
    print()
    print('Depart:  {}'.format(dep_ap))
    print('Status:  {}'.format(dep_status))
    print('Time:    {}'.format(dep_sch))
    print()
    print('Arrive:  {}'.format(arr_ap))
    print('Status:  {}'.format(arr_status))
    print('Time:    {}'.format(arr_sch))


def get_flight_num():
    """
    :return: No return, choose between airlines and choose flight number
    """
    while True:
        airline = input('Air Canada (AC) or Westjet (WJ)?: ')
        if airline.lower() in ['ac', 'air canada', 'wj', 'westjet']:
            break
    while True:
        flight_num = input('Enter flight #: ')
        if flight_num.isdigit() and 0 < len(flight_num) < 5:
            break
    if airline.lower() in ['ac', 'air canada']:
        air_canada_flight_info(flight_num)
    else:
        westjet_flight_info(flight_num)


if __name__ == '__main__':
    if not path.exists(filename):  # if airport codes file DOES NOT exist
        iata_scraper()        # create airport codes JSON file

    with open(filename) as file:   # open airport codes JSON file
        airport_codes = load(file)

    user_agents = random_ua()      # default == windows user-agents

    get_flight_num()
