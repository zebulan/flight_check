from bs4 import BeautifulSoup
from json import dump
from os import path
from requests import get
from string import ascii_uppercase
from user_agents import random_ua


def iata_scraper():
    """
    :return: Creates a JSON file of IATA airport codes from Wikipedia
    """
    ua_list = random_ua()  # shuffled list of windows browsers
    wiki = 'https://en.wikipedia.org/wiki/List_of_airports_by_IATA_code:_'
    frame = {0: 'IATA', 2: 'Airport', 3: 'Location'}  # attributes to save
    airport_codes = dict()
    for i, letter in enumerate(ascii_uppercase):
        r = get(wiki + letter, headers={'User-Agent': ua_list[i]})
        soup = BeautifulSoup(r.text)
        for row in soup.find_all('tr'):
            key = ''  # only empty until index below == 0
            for dex, col in zip(range(6), (a.text for a in row.find_all('td'))):
                if dex == 0:        # first column is the IATA code
                    key = col       # key is the IATA airport code
                    airport_codes[key] = dict()
                elif dex in frame:  # other attributes are Airport and Location
                    airport_codes[key][frame[dex]] = col

    with open(filename, 'w') as file:
        dump(airport_codes, file)  # save IATA codes as a JSON file


filename = 'iata_airport_codes.json'  # filename for json output file

if __name__ == '__main__':
    if not path.exists(filename):
        iata_scraper()
    else:
        print('\'{}\' already exists.'.format(filename))
        while True:
            recreate = input('Overwrite and update? (Y/N) ')
            if recreate.lower() == 'y' or recreate.lower()[0] == 'y':
                iata_scraper()
            elif recreate.lower() == 'n':
                break
