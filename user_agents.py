from bs4 import BeautifulSoup
from os import path
from random import shuffle
from requests import get


def random_ua(win=True, mac=False, linux=False, unix=False):
    """
    :param win: Windows Browser
    :param mac: Mac Browser
    :param linux: Linux Browser
    :param unix: Unix Browser
    :return: Shuffled list of User-Agents.
             Defaults to Windows User-Agents if all are False.
    """
    ua_filename = 'all_user_agents.xml'
    if not path.exists(ua_filename):  # download XML file if nonexistent
        url = 'http://techpatterns.com/downloads/firefox/useragentswitcher.xml'
        r = get(url, stream=True)
        with open(ua_filename, 'wb') as file:
            for chunk in r.iter_content(128):
                file.write(chunk)

    with open(ua_filename, 'r') as file:
        soup = BeautifulSoup(file, 'xml')

    agents = list()
    browser_os = list()
    if not win and not mac and not linux and not unix:  # all False
        win = True  # default back to windows

    if win:
        browser_os.append('Browsers - Windows')
    if mac:
        browser_os.append('Browsers - Mac')
    if linux:
        browser_os.append('Browsers - Linux')
    if unix:
        browser_os.append('Browsers - Unix')

    for os in browser_os:
        for a in soup.find_all(description=os):
            agents.extend([b['useragent'] for b in a.find_all('useragent')])
    shuffle(agents)
    return agents

if __name__ == '__main__':
    print(random_ua())  # default returns windows user-agents
